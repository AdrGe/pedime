import 'package:flutter/material.dart';

class AppColors {
  AppColors._(); // this basically makes it so you can instantiate this class

  static const Map<int, Color> pedimeTheme = const <int, Color>{
    50: Color(0xFFE9F0F1),
    100: Color(0xFFC7D9DD),
    200: Color(0xFFA2C0C7),
    300: Color(0xFF7DA7B0),
    400: Color(0xFF61949F),
    500: Color(0xFF45818E),
    600: Color(0xFF3E7986),
    700: Color(0xFF366E7B),
    800: Color(0xFF2E6471),
    900: Color(0xFF1F515F),
  };

  static const MaterialColor pedimeThemeAccent =
      MaterialColor(_pedimeThemeAccentValue, <int, Color>{
    100: Color(0xFF9FE9FF),
    200: Color(_pedimeThemeAccentValue),
    400: Color(0xFF39D2FF),
    700: Color(0xFF1FCCFF),
  });
  static const int _pedimeThemeAccentValue = 0xFF6CDDFF;
}
