import 'package:flutter/material.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';

class Slides {
  Slides._();

  static List<PageViewModel> onBoard = [
    PageViewModel(
        pageColor: Color(0xFF03A9F4),
        // iconImageAssetPath: 'assets/air-hostess.png',
        bubble: Image.asset('assets/images/engranaje.png'),
        title: Text(
          '¡ Bienvenid@ !',
          style: TextStyle(fontSize: 30, fontWeight: FontWeight.w800),
        ),
        body: Text(
          'Vamos a arrancar con PediMe. Primero la tenés que configurar. Ingresá el NOMBRE que va a aparecer en los pedidos...',
        ),
        titleTextStyle: TextStyle(fontSize: 24, color: Colors.white),
        bodyTextStyle: TextStyle(fontSize: 18, color: Colors.white),
        mainImage: Image.asset(
          'assets/images/slide2.webp',
          height: 285.0,
          width: 285.0,
          alignment: Alignment.center,
        )),
    PageViewModel(
      pageColor: Color(0xFFd65abb),
      iconImageAssetPath: 'assets/images/bubble_dire.png',
      body: Text(
          '...después ingresá la DIRECCIÓN y los datos necesarios para que te encuentren...'),
      title: Text(
        'Dirección',
        style: TextStyle(fontSize: 30, fontWeight: FontWeight.w800),
      ),
      mainImage: Image.asset(
        'assets/images/slide3.webp',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      titleTextStyle: TextStyle(fontSize: 24, color: Colors.white),
      bodyTextStyle: TextStyle(fontSize: 18, color: Colors.white),
    ),
    PageViewModel(
      pageColor: Color(0xFF45818E),
      title: Text('Ciudad'),
      titleTextStyle: TextStyle(
          fontSize: 30, fontWeight: FontWeight.w800, color: Colors.white),
      mainImage: Image.asset(
        'assets/images/slide4.webp',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      body: Text(
        'Finalmente, eligiendo tu CIUDAD vas a encontrar todos los comercios que están en la app. Dale!',
      ),
      bodyTextStyle: TextStyle(fontSize: 18, color: Colors.white),
      iconImageAssetPath: 'assets/images/bubble_ciudad.png',
    ),
  ];

  static List<PageViewModel> howTo = [
    PageViewModel(
      pageColor: Color(0xFF32a852),
      title: Text('¿Como se usa? 🤔'),
      titleTextStyle: TextStyle(fontSize: 28),
      mainImage: Image.asset(
        'assets/images/howTo1.webp',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      body: Text(
          'Tenés una PESTAÑA de comercios por RUBRO y otra PESTAÑA de comercios en tu CIUDAD'),
    ),
    PageViewModel(
      pageColor: Color(0xFFd65abb),
      title: Text('Lista de Comercios'),
      titleTextStyle: TextStyle(fontSize: 28),
      mainImage: Image.asset(
        'assets/images/howTo2.webp',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      body: Text('Seleccionas un comercio 🏪 para ver sus productos'),
    ),
    PageViewModel(
      pageColor: Color(0xFF03A9F4),
      title: Text('Lista de Productos'),
      titleTextStyle: TextStyle(fontSize: 29),
      mainImage: Image.asset(
        'assets/images/howTo3.webp',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      body: Text('Elegis de la lista los productos 🍔 que querés pedir 🤤'),
    ),
    PageViewModel(
      pageColor: Color(0xFF0dbabd),
      title: Text('Carrito'),
      titleTextStyle: TextStyle(fontSize: 29),
      mainImage: Image.asset(
        'assets/images/howTo4.webp',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      body: Text(
          'Una vez que completaste el carrito 🛒, lo envías por Whatsapp o llamás para pedir... 🛵'),
    ),
  ];

  static List<PageViewModel> comer = [
    PageViewModel(
      pageColor: Color(0xFFd65abb),
      title: Text('Datos del Comercios'),
      titleTextStyle: TextStyle(fontSize: 28),
      mainImage: Image.asset(
        'assets/images/comer1.webp',
        height: 355.0,
        width: 355.0,
        alignment: Alignment.center,
      ),
      body: Text(
          'Si tocas en el logo del comercio 🏪 vas a encontrar todos sus datos.'),
    ),
    PageViewModel(
      pageColor: Color(0xFF03A9F4),
      title: Text('Detalle del comercio'),
      titleTextStyle: TextStyle(fontSize: 29),
      mainImage: Image.asset(
        'assets/images/comer2.webp',
        height: 185.0,
        width: 355.0,
        alignment: Alignment.center,
      ),
      body: Text(
          'Barra Detalles: podés ver los métodos de pago que acepta, si hace delivery o takeAway, y si podés pedir por Tel. o Whatsapp'),
    ),
    PageViewModel(
      pageColor: Color(0xFF0dbabd),
      title: Text('Agregar contacto'),
      titleTextStyle: TextStyle(fontSize: 29),
      mainImage: Image.asset(
        'assets/images/comer3.webp',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      body: Text(
          'Tocando en el botón de abajo, podés crear automaticamente el contacto de este comercio!'),
    ),
  ];
}
