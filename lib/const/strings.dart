class Strings {
  Strings._();

  //General
  static const String appName = "PediMe";
  static const String noHay = "Nada por aquí, nada por allá :(";
  static const String error = "Algo falló, vuelva a intentar";

  //Settings
  static const String config = "Configuración";
  static const String ciudad = "Ciudad: ";
  static const String ciudadTip = "Ciudad donde te encuetras y quieras pedir.";
  static const String nomb = "Tu Nombre: ";
  static const String nombTip = "El nombre que va a aparecer en tu pedido";
  static const String dire = "Dirección: ";
  static const String direTip = "Dirección donde llevar el pedido";
  static const String refe = "Referencia: ";
  static const String refeTip =
      "por Ej: después del árbol en medio de la calle ;)";

  // Carrito
  static const String carrito = "Su Pedido";
  static const String saludo = "Hola, soy ";

  // Comercios
  static const String comerTit = "Lista de Comercios";
  static const String comerXr = "Comercios del rubro";

  //Productos
  static const String prodsTit = "Productos del Comercio";
  static const String prodsSub = "Comprame algo, dale";

// C

}
