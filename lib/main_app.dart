import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:pedime/const/app_theme.dart';
import 'package:pedime/routes.dart';
import 'package:pedime/ui/home.dart';
import 'package:pedime/ui/onBoarding.dart';
// import 'package:pedime/ui/set_prefs.dart';
import 'models/user.dart';

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    User _localUser = GetIt.I.get<User>();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: themeData,
      home: _localUser.configurado ? HomeScreen() : OnBoarding(),
      routes: Routes.routes,
    );
  }
}
