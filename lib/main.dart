import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:pedime/main_app.dart';
import 'package:pedime/models/user.dart';
import 'package:pedime/splash_app.dart';
import 'package:provider/provider.dart';
import 'data/carrito.dart';
import 'data/net/firebase_db.dart';

GetIt locator = GetIt.instance;

void setupSingletons() async {
  locator.registerLazySingleton<User>(() => User());
}

void main() {
  setupSingletons();
  WidgetsFlutterBinding.ensureInitialized();

  runApp(
    SplashApp(
      key: UniqueKey(),
      onInitializationComplete: () => runMainApp(),
    ),
  );
}

void runMainApp() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => CartModel()),
        Provider(create: (context) => FirestoreDatabase()),
      ],
      child: MainApp(),
    ),
  );
}
