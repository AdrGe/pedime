//
import 'package:flutter/material.dart';

/// Helper class for device related operations.
///
class DeviceUtils {
  ///
  /// hides the keyboard if its already open
  ///
  static hideKeyboard(BuildContext context) {
    FocusScope.of(context).unfocus();
  }

  ///
  /// accepts a double [scale] and returns scaled sized based on the screen
  /// orientation
  ///
  static double getScaledSize(BuildContext context, double scale) =>
      scale *
      (MediaQuery.of(context).orientation == Orientation.portrait
          ? MediaQuery.of(context).size.width
          : MediaQuery.of(context).size.height);

  ///
  /// accepts a double [scale] and returns scaled sized based on the screen
  /// width
  ///
  static double getScaledWidth(BuildContext context, double scale) =>
      scale * MediaQuery.of(context).size.width;

  ///
  /// accepts a double [scale] and returns scaled sized based on the screen
  /// height
  ///
  static double getScaledHeight(BuildContext context, double scale) =>
      scale * MediaQuery.of(context).size.height;

  ///TODO: gets padding of the screen
//static getPadding(BuildContext context) => MediaQuery.of(context).padding;

  /// height without SafeArea
  static double getUnsafeHeight(BuildContext context) {
    var padding = MediaQuery.of(context).padding;
    double height = getScaledHeight(context, 1) - padding.top - padding.bottom;
    return height;
  }

  /// height without status bar
  static double getHeightWithoutStatusBar(BuildContext context) {
    var padding = MediaQuery.of(context).padding;
    double height = getScaledHeight(context, 1) - padding.top;
    return height;
  }

  /// height without status and toolbar
  static double getHeightWithoutStatusAnToolBar(BuildContext context) {
    var padding = MediaQuery.of(context).padding;
    double height = getScaledHeight(context, 1) - padding.top - kToolbarHeight;
    return height;
  }
}
