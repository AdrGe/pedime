/// Flutter icons PediMe
/// Copyright (C) 2020 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// To use this font, place it in your fonts/ directory and include the
/// following in your pubspec.yaml
///
/// flutter:
///   fonts:
///    - family:  PediMe
///      fonts:
///       - asset: fonts/PediMe.ttf
///
///
/// * Material Design Icons, Copyright (C) Google, Inc
///         Author:    Google
///         License:   Apache 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
///         Homepage:  https://design.google.com/icons/
/// * Typicons, (c) Stephen Hutchings 2012
///         Author:    Stephen Hutchings
///         License:   SIL (http://scripts.sil.org/OFL)
///         Homepage:  http://typicons.com/
/// * Linecons, Copyright (C) 2013 by Designmodo
///         Author:    Designmodo for Smashing Magazine
///         License:   CC BY ()
///         Homepage:  http://designmodo.com/linecons-free/
/// * Entypo, Copyright (C) 2012 by Daniel Bruce
///         Author:    Daniel Bruce
///         License:   SIL (http://scripts.sil.org/OFL)
///         Homepage:  http://www.entypo.com
/// * Elusive, Copyright (C) 2013 by Aristeides Stathopoulos
///         Author:    Aristeides Stathopoulos
///         License:   SIL (http://scripts.sil.org/OFL)
///         Homepage:  http://aristeides.com/
/// * Linearicons Free, Copyright (C) Linearicons.com
///         Author:    Perxis
///         License:   CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/)
///         Homepage:  https://linearicons.com
/// * Font Awesome 5, Copyright (C) 2016 by Dave Gandy
///         Author:    Dave Gandy
///         License:   SIL (https://github.com/FortAwesome/Font-Awesome/blob/master/LICENSE.txt)
///         Homepage:  http://fortawesome.github.com/Font-Awesome/
/// * Font Awesome 4, Copyright (C) 2016 by Dave Gandy
///         Author:    Dave Gandy
///         License:   SIL ()
///         Homepage:  http://fortawesome.github.com/Font-Awesome/
/// * Octicons, Copyright (C) 2020 by GitHub Inc.
///         Author:    GitHub
///         License:   MIT (http://opensource.org/licenses/mit-license.php)
///         Homepage:  https://primer.style/octicons/
///
import 'package:flutter/widgets.dart';

class PediMe {
  PediMe._();

  static const _kFontFam = 'PediMe';
  static const _kFontPkg = null;

  static const IconData motorcycle =
      IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData widgets =
      IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData store =
      IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData remove_shopping_cart =
      IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData add_shopping_cart =
      IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData basket =
      IconData(0xe805, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData attach_money =
      IconData(0xe806, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData wallet =
      IconData(0xe807, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData money_1 =
      IconData(0xe808, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData credit_card =
      IconData(0xe809, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData local_phone =
      IconData(0xe80a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData shopping_basket =
      IconData(0xe80b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData pan_tool =
      IconData(0xe80c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData delete =
      IconData(0xe80d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData trash =
      IconData(0xe80e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData trash_1 =
      IconData(0xe80f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData adult =
      IconData(0xe810, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData person =
      IconData(0xe811, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData location_city =
      IconData(0xe812, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData location_on =
      IconData(0xe813, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData location =
      IconData(0xe814, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData palette =
      IconData(0xe815, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData do_not_disturb_on =
      IconData(0xe816, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData store_1 =
      IconData(0xe82d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData cart =
      IconData(0xe82e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData phone_handset =
      IconData(0xe830, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData hand =
      IconData(0xe8a5, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData credit_card_1 =
      IconData(0xf09d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData money =
      IconData(0xf0d6, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData whatsapp =
      IconData(0xf232, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  // static const IconData shopping_basket = IconData(0xf291, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData location_1 =
      IconData(0xf353, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData money_bill_wave =
      IconData(0xf53a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData palette_1 =
      IconData(0xf53f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData city =
      IconData(0xf64f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
