import 'package:flutter/material.dart';

class GzIncrementador extends StatelessWidget {
  final int cant;
  final VoidCallback onCantSelected;

  final Function(int) onCantChange;

  GzIncrementador({
    @required this.cant,
    @required this.onCantChange,
    this.onCantSelected,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        GestureDetector(
          child: Container(
            padding: const EdgeInsets.all(5.0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Theme.of(context).buttonColor,
            ),
            child: Icon(
              Icons.remove,
              color: Colors.white,
            ),
          ),
          onTap: () {
            onCantChange(1);
          },
        ),
        SizedBox(width: 15),
        Text(
          "$cant",
          style: Theme.of(context).textTheme.headline3,
        ),
        SizedBox(width: 15),
        GestureDetector(
          child: Container(
            padding: const EdgeInsets.all(5.0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Theme.of(context).buttonColor,
            ),
            child: Icon(
              Icons.add,
              color: Colors.white,
            ),
          ),
          onTap: () {
            onCantChange(-1);
          },
        ),
      ],
    );
  }
}
