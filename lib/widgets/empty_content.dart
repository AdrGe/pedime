import 'package:flutter/material.dart';

class EmptyContent extends StatelessWidget {
  const EmptyContent({@required this.title, @required this.message});
  final String title;
  final String message;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 22.0, color: Colors.black),
          ),
          SizedBox(height: 10),
          Text(
            message,
            style: TextStyle(fontSize: 32.0),
          ),
        ],
      ),
    );
  }
}
