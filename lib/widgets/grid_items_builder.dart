import 'package:flutter/material.dart';
import 'package:pedime/const/strings.dart';
import 'package:pedime/widgets/empty_content.dart';

typedef ItemWidgetBuilder<T> = Widget Function(BuildContext context, T item);

class GridItemsBuilder<T> extends StatelessWidget {
  const GridItemsBuilder({
    Key key,
    @required this.snapshot,
    @required this.itemBuilder,
  }) : super(key: key);
  final AsyncSnapshot<List<T>> snapshot;
  final ItemWidgetBuilder<T> itemBuilder;

  @override
  Widget build(BuildContext context) {
    if (snapshot.hasData) {
      final List<T> items = snapshot.data;
      if (items.isNotEmpty) {
        return _buildList(items);
      } else {
        return EmptyContent(
          title: 'Vacío',
          message: 'El vacío está completo',
        );
      }
    } else if (snapshot.hasError) {
      return EmptyContent(
        title: Strings.error,
        message: Strings.noHay,
      );
    }
    return Center(child: CircularProgressIndicator());
  }

  Widget _buildList(List<T> items) {
    return GridView.extent(
        maxCrossAxisExtent: 200,
        crossAxisSpacing: 4.0,
        mainAxisSpacing: 4.0,
        children: <Widget>[
          ListView.builder(
            itemCount: items.length + 2,
            itemBuilder: (context, index) {
              if (index == 0 || index == items.length + 1) {
                return Container(); // zero height: not visible
              }
              return itemBuilder(context, items[index - 1]);
            },
          )
        ]);
  }
}
