import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pedime/ui/los_comercios.dart';
import 'package:pedime/ui/pedido.dart';
import 'package:pedime/ui/rubros.dart';
// import 'package:badges/badges.dart';
import 'package:pedime/ui/set_prefs.dart';

class HomeScreen extends StatelessWidget {
  // static final myTabbedPageKey =  GlobalKey<HomeScreen()>();
  // static final myTabbedPageKey = GlobalKey<this>();

  @override
  Widget build(BuildContext context) {
// TODO:poner este badge y leerlo desde un controller de GetX
    // bool noBadge = true;
    // final cart = Provider.of<CartModel>(context).items;

/*     if (cart.length == 0) {
      noBadge = true;
    } else {
      noBadge = false;
    } */

    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
            actions: <Widget>[
              IconButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => SetPrefs(),
                  ));
                },
                icon: Icon(Icons.settings),
              )
            ],
            bottom: TabBar(
              tabs: [
                Tab(icon: Icon(Icons.category), text: 'Rubros'),
                Tab(
                  icon: Icon(Icons.store),
                  text: 'Comercios',
                ),
/*                 noBadge
                    ?  */
                Tab(
                  icon: Icon(Icons.shopping_cart),
                  text: 'Pedido',
                )
/*                     : Tab(
                        child: Badge(
                          badgeContent: Text('1'),
                          child: Icon(Icons.shopping_basket),
                          badgeColor: Colors.red,
                          position: BadgePosition.topRight(),
                          toAnimate: true,
                          animationType: BadgeAnimationType.scale,
                        ),
                        text: 'Pedido',
                      ), */
              ],
            ),
            title: Text(
              'PediMe',
              style: GoogleFonts.anton(
                textStyle: Theme.of(context).textTheme.headline4,
                color: Color(0xFF45818E),
                shadows: [
                  Shadow(
                    color: Colors.black,
                    offset: Offset(-3.0, 3.0),
                  ),
                ],
              ),
            )),
        body: TabBarView(
          children: [
            RubrosScreen(),
            LosComercios(),
            VerCarrito(),
          ],
        ),
      ),
    );
  }
}
