import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:pedime/const/strings.dart';
import 'package:pedime/models/user.dart';
import 'package:pedime/ui/acerca_de.dart';
import 'package:pedime/ui/ayudaScreen.dart';
import 'package:pedime/ui/home.dart';
/* import 'package:pedime/ui/howTo.dart';
import 'package:pedime/ui/onBoarding.dart'; */

// TODO:AGregar cambio de theme light o dark

class SetPrefs extends StatefulWidget {
  @override
  _SetPrefsState createState() => _SetPrefsState();
}

class _SetPrefsState extends State<SetPrefs> {
  int nivelConfiguracion = 1;

  Widget _ingAlertDialog(BuildContext context, String pref) {
    User usuario = GetIt.I.get<User>();

/*     @override
    void initState() {
      if (usuario.configurado != null) {
        nivelConfiguracion + 33;
      }
      if (usuario.nombre != null) {
        nivelConfiguracion + 33;
      }
      if (usuario.dire != null) {
        nivelConfiguracion + 33;
      }

      super.initState();
    } */

    String tit;
    if (pref == 'nombre') {
      tit = Strings.nomb;
    } else {
      tit = Strings.dire;
    }
    return AlertDialog(
      title: Text(tit),
      content: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            TextField(
              style: TextStyle(fontSize: 20, color: Colors.black54),
              controller: TextEditingController(
                  text: pref == 'nombre'
                      ? usuario.nombre ?? ''
                      : usuario.dire ?? ''),
              textCapitalization: TextCapitalization.sentences,
              onSubmitted: (text) {
                if (pref == 'nombre') {
                  usuario.nuevoNombre(text);
                } else {
                  usuario.nuevaDire(text);
                }
/*                 if (nivelConfiguracion < 35 || nivelConfiguracion < 66) {
                  setState(() {
                    nivelConfiguracion + 33;
                  });
                }                ; */
                Navigator.pop(context);
              },
            ),
            SizedBox(height: 20),
            Text('Con este nombre saldrán tus pedidos por whatsapp'),
          ],
        ),
      ),
    );
  }

  _ciudadesAlertDialog(BuildContext context) {
    User usuario = GetIt.I.get<User>();
    // final cart = GetIt.I.get<CartModel>();
    return AlertDialog(
      title: Text('Ciudades'),
      content: StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance
            .collection('ciudades')
            .orderBy('nombre')
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) return Text('Error: $snapshot.error');
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(
                child: CircularProgressIndicator(),
              );
            default:
              return Container(
                height: 400.0,
                width: 300.0,
                child: SingleChildScrollView(
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (BuildContext context, index) {
                        DocumentSnapshot ciudad =
                            snapshot.data.documents[index];
                        return InkWell(
                          child: ListTile(title: Text(ciudad['nombre'])),
                          onTap: () {
                            usuario.cambiaConf();
                            usuario.nuevaCiudad(ciudad['nombre']);
/*                             if (cart.items.length > 0) {
                              Flushbar(
                                onTap: (_) {
                                  cart.removeAll();
                                },
                                backgroundColor: Theme.of(context).primaryColor,
                                title: 'Carrito',
                                message:
                                    'Hay elementos en el carrito. Tap para borrar',
                                duration: Duration(seconds: 6),
                              )..show(context);
                             }*/
/*                             setState(() {
                              nivelConfiguracion + 33;
                            }); */
                            Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                builder: (BuildContext context) => HomeScreen(),
                              ),
                              (route) => false,
                            );
                          },
                        );
                      }),
                ),
              );
          }
        },
      ),
    );
  }

/*   Widget _muestraNivel() {
    return AlertDialog(
      title: Text('Nivel de Configuración'),
      content:
          Text('Su nivel de Configuración está en el $nivelConfiguracion %'),
    );
  } */

  @override
  Widget build(BuildContext context) {
    // final usuario = Provider.of<User>(context);
    User usuario = GetIt.I.get<User>();
    return Scaffold(
      appBar: AppBar(title: Text(Strings.config)),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: <Widget>[
                SizedBox(height: 25),
                ListTile(
                  leading: Icon(
                    Icons.person_pin,
                    color: Colors.cyan[800],
                    size: 30,
                  ),
                  // contentPadding: EdgeInsets.symmetric(),
                  title: Text(usuario.nombre ?? 'Nombre'),
                  subtitle: Text('Tu nombre tal como aparecerá en el pedido'),
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return _ingAlertDialog(context, 'nombre');
                      },
                    );
                  },
                ),
                SizedBox(height: 15),
                ListTile(
                  leading: Icon(Icons.location_on,
                      color: Colors.cyan[800], size: 30),
                  title: Text(usuario.dire ?? 'Dirección'),
                  subtitle: Text('La dirección donde se entregará el pedido'),
                  onTap: () {
                    showDialog(
                      context: context,
                      //barrierDismissible: false, // user must tap button!
                      builder: (BuildContext context) {
                        return _ingAlertDialog(context, 'dire');
                      },
                    );
                  },
                ),
                SizedBox(height: 15),
                ListTile(
                  leading: Icon(Icons.location_city,
                      color: Colors.cyan[800], size: 30),
                  title: Text(usuario.ciudad ?? 'Ciudad'),
                  subtitle: Text('La ciudad que queres consultar por pedidos'),
                  onTap: () {
                    showDialog(
                        context: context,
                        barrierDismissible: false, // user must tap button!
                        builder: (BuildContext context) {
                          return _ciudadesAlertDialog(context);
                        });
                  },
                ),
                SizedBox(height: 15),
                /* ListTile(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return _muestraNivel();
                        });
                  },
                  leading: Icon(Icons.tune, color: Colors.cyan[800], size: 30),
                  title: Text('Nivel de Configuración'),
                  subtitle: Row(
                    // mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      this.nivelConfiguracion > 90
                          ? Container(
                              width: 20,
                              height: 8,
                              child: ColoredBox(color: Colors.green))
                          : Container(
                              width: 20,
                              height: 8,
                              child: ColoredBox(color: Colors.grey[350])),
                      this.nivelConfiguracion < 67
                          ? Container(
                              child: ColoredBox(color: Colors.yellow),
                              height: 8,
                              width: 20)
                          : Container(
                              width: 20,
                              height: 8,
                              child: ColoredBox(color: Colors.grey[350])),
                      this.nivelConfiguracion < 34
                          ? Container(
                              child: ColoredBox(color: Colors.red),
                              height: 8,
                              width: 20)
                          : Container(
                              width: 20,
                              height: 8,
                              child: ColoredBox(color: Colors.grey[350])),
                    ],
                  ),
                ), */
                SizedBox(height: 50),
                ListTile(
                  leading:
                      Icon(Icons.live_help, color: Colors.cyan[800], size: 30),
                  title: Text('Ayuda'),
                  subtitle: Text('Tenés dudas de como usar la aplicación'),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AyudaScreen(),
                      ),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(Icons.brightness_auto,
                      color: Colors.cyan[800], size: 30),
                  title: Text('Acerca de'),
                  subtitle: Text('Toda la información sobre PediMe'),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AcercaScreen(),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
