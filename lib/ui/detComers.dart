import 'package:flutter/material.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';
import 'package:pedime/const/slides.dart';
import 'package:pedime/ui/ayudaScreen.dart';

class DetallesComer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IntroViewsFlutter(
      Slides.comer,
      // background: Colors.black.withOpacity(0.6),
      showNextButton: true,
      skipText: Text('Omitir'),
      nextText: Text('>'),
      backText: Text('<'),
      showBackButton: true,
      doneText: Text('Listo'),
      onTapDoneButton: () {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => AyudaScreen(),
          ),
          (route) => false,
        );
      },
    );
  }
}
