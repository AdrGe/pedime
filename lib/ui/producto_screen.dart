import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:pedime/data/carrito.dart';
import 'package:pedime/models/producto.dart';
import 'package:provider/provider.dart';
import 'package:pedime/util/device.dart';
import 'package:neon/neon.dart';

class DescripcionProducto extends StatelessWidget {
  const DescripcionProducto(
      {@required this.comId, @required this.prod, @required this.ws});
  final String comId;
  final Producto prod;
  final bool ws;

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<CartModel>(context);
    final ancho = DeviceUtils.getScaledWidth(context, 1);
    final alto = DeviceUtils.getScaledHeight(context, 1);

    return Scaffold(
      backgroundColor: Colors.grey[100],
      body: CustomScrollView(
        physics: const BouncingScrollPhysics(),
        slivers: <Widget>[
          SliverAppBar(
            expandedHeight: 200.0,
            stretch: true,
            pinned: true,
            shape: ContinuousRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30))),
            flexibleSpace: FlexibleSpaceBar(
              stretchModes: <StretchMode>[
                StretchMode.zoomBackground,
                StretchMode.blurBackground,
                StretchMode.fadeTitle,
              ],
              centerTitle: true,
              title: Neon(
                text: prod.nombre,
                color: Colors.lightGreen,
                fontSize: 22,
                font: NeonFont.LasEnter,
                flickeringText: true,
                flickeringLetters: null,
                glowingDuration: Duration(seconds: 280),
              ),
              background: FadeInImage.assetNetwork(
                fit: BoxFit.fill,
                image: prod.img,
                placeholder: "assets/images/box.png",
                fadeInDuration: Duration(milliseconds: 500),
                fadeOutDuration: Duration(milliseconds: 400),
                fadeOutCurve: Curves.easeInOut,
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 30),
                          Text(
                            'Descripción:',
                            textScaleFactor: 1.3,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.speaker_notes,
                                  size: 30,
                                  color: Theme.of(context).primaryColor,
                                ),
                                SizedBox(width: 10),
                                Expanded(
                                  child: Text(
                                    prod.desc,
                                    softWrap: true,
                                    textScaleFactor: 1.2,
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 20),
                          Text(
                            'Precio:',
                            textScaleFactor: 1.3,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.local_atm,
                                  size: 30,
                                  color: Theme.of(context).primaryColor,
                                ),
                                SizedBox(width: 10),
                                Text(
                                  '${prod.precio.toString().replaceAll(".", ",")}\$',
                                  textScaleFactor: 1.2,
                                  style: TextStyle(color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                          SizedBox(height: alto * 0.1),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: alto * 0.1),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 50,
                        width: ancho - 16,
                        child: RaisedButton(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Quiero pedir!",
                                  style: Theme.of(context).textTheme.subtitle1),
                              SizedBox(width: 15),
                              Icon(
                                Icons.pan_tool,
                                size: 30, color: Colors.black26,
                                // color: Theme.of(context).textSelectionColor,
                              ),
                            ],
                          ),
                          onPressed: () =>
                              _agProd(comId, cart, prod, context, ws),
                          color: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
/*       bottomNavigationBar: Container(
        color: Colors.cyan[800],
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 2),
            FlatButton(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Quiero pedir!",
                      style: Theme.of(context).textTheme.subtitle1),
                  SizedBox(width: 15),
                  Icon(
                    Icons.pan_tool,
                    size: 30, color: Colors.black26,
                    // color: Theme.of(context).textSelectionColor,
                  ),
                ],
              ),
              onPressed: () => _agProd(comId, cart, prod, context, ws),
            ),
            SizedBox(height: 2)
          ],
        ),
      ), */
    );
  }

  void _agProd(String comId, CartModel carri, Producto prod,
      BuildContext context, bool wsapp) {
    if (carri.comercio != null && comId != carri.comercio) {
      Flushbar(
        backgroundColor: Theme.of(context).errorColor,
        title: 'Error',
        message: 'No se puede agregar productos de distintos comercios 🤷',
        duration: Duration(seconds: 4),
      )..show(context);
    } else {
      carri.add(prod);
      carri.comercio = comId;
      carri.wsapp = wsapp;
      Flushbar(
        backgroundColor: Theme.of(context).accentColor,
        title: 'Agregado',
        message: 'Se agregó el producto al pedido 👍',
        duration: Duration(seconds: 3),
      )..show(context);
    }
  }
}
