import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:pedime/models/super_comercio.dart';
import 'package:pedime/util/device.dart';
import 'package:pedime/util/pedime_icons.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:http/http.dart' as http;
import 'package:neon/neon.dart';

class DescripcionComercio extends StatelessWidget {
  const DescripcionComercio({@required this.comId});

  final String comId;

  Stream<Comercio> _getCom() {
    return Firestore.instance
        .collection("comercios")
        .document(comId)
        .get()
        .then((snapshot) {
      try {
        return Comercio.fromMap(snapshot.data);
      } catch (e) {
        print(e);
        return null;
      }
    }).asStream();
  }

  @override
  Widget build(BuildContext context) {
    final ancho = DeviceUtils.getScaledWidth(context, 1);
    // final comer = _getCom();
    return Scaffold(
      backgroundColor: Colors.grey[100],
      body: StreamBuilder(
        stream: _getCom(),
        builder: (context, snapshot) {
          Comercio com = snapshot.data;
          if (snapshot.hasData) {
            return CustomScrollView(
              physics: const BouncingScrollPhysics(),
              slivers: <Widget>[
                SliverAppBar(
                  expandedHeight: 200.0,
                  stretch: true,
                  pinned: true,
                  shape: ContinuousRectangleBorder(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(30),
                          bottomRight: Radius.circular(30))),
                  flexibleSpace: FlexibleSpaceBar(
                    stretchModes: <StretchMode>[
                      StretchMode.zoomBackground,
                      StretchMode.blurBackground,
                      StretchMode.fadeTitle,
                    ],
                    centerTitle: true,
                    title: Neon(
                      text: com.nombre,
                      color: Colors.lightGreen,
                      fontSize: 25,
                      font: NeonFont.LasEnter,
                      // font: NeonFont.TextMeOne,
                      // font: NeonFont.Beon,
                      // font: NeonFont.Monoton
                      // font: NeonFont.Automania,
                      // font: NeonFont.Cyberpunk,
                      // font: NeonFont.Membra
                      // font: NeonFont.Night-Club-70s,
                      // font: NeonFont.Samarin,
                      flickeringText: true,
                      flickeringLetters: null,
                      glowingDuration: Duration(seconds: 280),
                    ),
                    background: FadeInImage.assetNetwork(
                      fit: BoxFit.fill,
                      image: com.logo,
                      placeholder: "assets/images/box.png",
                      fadeInDuration: Duration(milliseconds: 500),
                      fadeOutDuration: Duration(milliseconds: 400),
                      fadeOutCurve: Curves.easeInOut,
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: ancho,
                        color: Colors.grey[350],
                        child: Column(
                          children: [
                            RichText(
                              text: TextSpan(
                                style: TextStyle(color: Colors.black),
                                children: <TextSpan>[
                                  TextSpan(
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 18),
                                      text: "Dirección: "),
                                  TextSpan(
                                      text: '${com.calle} ',
                                      style: TextStyle(fontSize: 18)),
                                  TextSpan(
                                      text: '${com.nro ?? 's/n'}, ',
                                      style: TextStyle(fontSize: 16)),
                                  TextSpan(
                                    text: com.ciudad,
                                    style: TextStyle(fontSize: 18),
                                  )
                                ],
                              ),
                            ),
                            RichText(
                              text: TextSpan(
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20),
                                children: <TextSpan>[
                                  TextSpan(
                                      text: 'Teléfono Fijo: ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 18)),
                                  TextSpan(
                                      text: '${com.tel ?? 'No'}',
                                      style: TextStyle(fontSize: 16)),
                                ],
                              ),
                            ),
                            // SizedBox(height: 7),
                            RichText(
                              text: TextSpan(
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20),
                                children: <TextSpan>[
                                  TextSpan(
                                      text: 'Whatsapp: ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 18)),
                                  TextSpan(
                                      text: '${com.cel ?? 'No'}',
                                      style: TextStyle(fontSize: 16)),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 25),
                      SingleChildScrollView(
                        child: Column(
                          children: <Widget>[
                            Text('Podés pagar con:',
                                style: Theme.of(context).textTheme.headline6),
                            Row(
                              children: <Widget>[
                                Icon(Icons.attach_money,
                                    color: Theme.of(context).accentColor),
                                Text(
                                  ' Efectivo',
                                  style: TextStyle(color: Colors.black54),
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Icon(Icons.credit_card,
                                    color: com.metPago.tc
                                        ? Theme.of(context).accentColor
                                        : Colors.grey[200]),
                                Text(
                                  ' Tarjeta de Crédito',
                                  style: com.metPago.tc
                                      ? TextStyle(color: Colors.black54)
                                      : TextStyle(
                                          decoration:
                                              TextDecoration.lineThrough,
                                          color: Colors.black26),
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Icon(Icons.credit_card,
                                    color: com.metPago.td
                                        ? Theme.of(context).accentColor
                                        : Colors.grey[200]),
                                Text(
                                  ' Tarjeta de Débito',
                                  style: com.metPago.td
                                      ? TextStyle(color: Colors.black54)
                                      : TextStyle(
                                          decoration:
                                              TextDecoration.lineThrough,
                                          color: Colors.black26),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Icon(Icons.mobile_screen_share,
                                    color: com.metPago.mp
                                        ? Theme.of(context).accentColor
                                        : Colors.grey[200]),
                                Text(
                                  ' Mercado Pago',
                                  style: com.metPago.mp
                                      ? TextStyle(color: Colors.black54)
                                      : TextStyle(
                                          decoration:
                                              TextDecoration.lineThrough,
                                          color: Colors.black26),
                                )
                              ],
                            ),
                            SizedBox(height: 9),
                            Divider(),
                            Text(' Tipos de Entrega',
                                style: Theme.of(context).textTheme.headline6),
                            Row(
                              children: <Widget>[
                                Icon(Icons.motorcycle,
                                    color: com.metDeli.moto
                                        ? Theme.of(context).accentColor
                                        : Colors.grey[200]),
                                Text(
                                  ' Envía por moto',
                                  style: com.metDeli.moto
                                      ? TextStyle(color: Colors.black54)
                                      : TextStyle(
                                          decoration:
                                              TextDecoration.lineThrough,
                                          color: Colors.black26),
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Icon(Icons.store,
                                    color: com.metDeli.retir
                                        ? Theme.of(context).accentColor
                                        : Colors.grey[200]),
                                Text(
                                  ' Retiras en puerta',
                                  style: com.metDeli.retir
                                      ? TextStyle(color: Colors.black54)
                                      : TextStyle(
                                          decoration:
                                              TextDecoration.lineThrough,
                                          color: Colors.black26),
                                )
                              ],
                            ),
                            SizedBox(height: 9),
                            Divider(),
                            Text('Pediles por',
                                style: Theme.of(context).textTheme.headline6),
                            Row(
                              children: <Widget>[
                                Icon(Icons.phone,
                                    color: com.pedirPor.telefono
                                        ? Theme.of(context).accentColor
                                        : Colors.grey[200]),
                                Text(
                                  ' Teléfono',
                                  style: com.pedirPor.telefono
                                      ? TextStyle(color: Colors.black54)
                                      : TextStyle(
                                          decoration:
                                              TextDecoration.lineThrough,
                                          color: Colors.black26),
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Icon(PediMe.whatsapp,
                                    color: com.pedirPor.whatsapp
                                        ? Theme.of(context).accentColor
                                        : Colors.grey[200]),
                                Text(
                                  ' Whatsapp',
                                  style: com.pedirPor.whatsapp
                                      ? TextStyle(color: Colors.black54)
                                      : TextStyle(
                                          decoration:
                                              TextDecoration.lineThrough,
                                          color: Colors.black26),
                                )
                              ],
                            ),
                            SizedBox(height: 9),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                com.pedirPor.whatsapp
                                    ? Container(
                                        height: 50,
                                        width: ancho - 16,
                                        child: RaisedButton(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text("Agregar contacto de",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1),
                                              SizedBox(width: 5),
                                              // Spacer(),
                                              Icon(PediMe.whatsapp),
                                            ],
                                          ),
                                          onPressed: () {
                                            _agregaContacto(context, com);
                                          },
                                          color: Theme.of(context).primaryColor,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                        ),
                                      )
                                    : Container(
                                        height: 50,
                                        width: ancho * 0.55,
                                        child: RaisedButton(
                                          color: Theme.of(context).primaryColor,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                          child: Text("Agregar contacto",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1),
                                          onPressed: () {
                                            _agregaContacto(context, com);
                                          },
                                        ),
                                      ),
                              ],
                            ),
                            SizedBox(height: 3)
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            );
          } else {
            return CustomScrollView(
              physics: const BouncingScrollPhysics(),
              slivers: <Widget>[
                SliverAppBar(
                  shape: ContinuousRectangleBorder(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(30),
                          bottomRight: Radius.circular(30))),
                  title: Text('Comercio'),
                ),
                SliverList(
                  delegate: SliverChildListDelegate(
                    [SizedBox(height: 30), LinearProgressIndicator()],
                  ),
                )
              ],
            );
          }
        },
      ),
      /* bottomNavigationBar: Container(
        color: Colors.cyan[800],
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 2),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[ 
                wp = comer.map((data) => data.pedirPor.whatsapp);
                                    ? Container(
                        height: 50,
                        width: ancho * 0.55,
                        child: RaisedButton(
                          child: Row(
                            children: <Widget>[
                              Text("Agregar contacto de",
                                  style: Theme.of(context).textTheme.subtitle1),
                              SizedBox(width: 15),
                              // Spacer(),
                              Icon(PediMe.whatsapp),
                            ],
                          ),
                          onPressed: () {
                            _agregaContacto(context, com);
                          },
                          color: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                        ),
                      )
                    : Container(
                        height: 50,
                        width: ancho * 0.55,
                        child: RaisedButton(
                          color: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Text("Agregar contacto",
                              style: Theme.of(context).textTheme.subtitle1),
                          onPressed: () {
                            _agregaContacto(context, com);
                          },
                        ),
                      ),
              ],
            ),
            SizedBox(height: 2)
          ],
        ),
      ), */
    );
  }

  _agregaContacto(BuildContext context, Comercio com) async {
    final PermissionStatus permissionStatus = await _getPermission();
    http.Response response = await http.get(com.logo);

    if (permissionStatus == PermissionStatus.granted) {
      Contact contacto = Contact();
      contacto.displayName = com.nombre;
      contacto.givenName = com.nombre;
      contacto.suffix = com.rubro;
      contacto.avatar = response.bodyBytes; //avt.buffer.asUint8List();
      contacto.postalAddresses = [
        PostalAddress.fromMap({
          'label': 'work',
          'street': '${com.calle} ${com.nro}',
          'city': '${com.ciudad}'
        })
      ];
      if (com.tel == null) {
        contacto.phones = [
          Item.fromMap({'label': 'mobile', 'value': '${com.cel.toString()}'})
        ];
      } else if (com.cel == null) {
        contacto.phones = [
          Item.fromMap({'label': 'work', 'value': '${com.tel.toString()}'})
        ];
      } else {
        contacto.phones = [
          Item.fromMap({'label': 'work', 'value': '${com.tel.toString()}'}),
          Item.fromMap({'label': 'mobile', 'value': '${com.cel.toString()}'})
        ];
      }
      await ContactsService.addContact(contacto).then((_) => Flushbar(
            backgroundColor: Theme.of(context).accentColor,
            title: 'Agregado',
            message: 'Se agregó el contacto 👍',
            duration: Duration(seconds: 4),
          )..show(context));
    } else {
      Flushbar(
        backgroundColor: Theme.of(context).errorColor,
        title: 'Problemas',
        message: 'No se agregó el contacto por falta de permisos',
        duration: Duration(seconds: 5),
      )..show(context);
    }
  }

//Check contacts permissions
  Future<PermissionStatus> _getPermission() async {
    final PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.denied) {
      final Map<Permission, PermissionStatus> permissionStatus =
          await [Permission.contacts].request();
      return permissionStatus[Permission.contacts] ??
          PermissionStatus.undetermined;
    } else {
      return permission;
    }
  }
}
