import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pedime/ui/detComers.dart';
import 'package:pedime/ui/home.dart';
import 'package:pedime/ui/howTo.dart';
import 'package:pedime/ui/onBoarding.dart';
import 'package:pedime/util/device.dart';
/* import 'package:pedime/util/pedime_icons.dart';
import 'package:pedime/widgets/app_icon.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart'; */

class AyudaScreen extends StatelessWidget {
  const AyudaScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final ancho = DeviceUtils.getScaledWidth(context, 1);
    final alto = DeviceUtils.getHeightWithoutStatusAnToolBar(context);

    return Scaffold(
      appBar: AppBar(
        title: InkWell(
          child: Text(
            'PediMe',
            style: GoogleFonts.anton(
              textStyle: Theme.of(context).textTheme.headline4,
              color: Color(0xFF45818E),
              shadows: [
                Shadow(
                  color: Colors.black,
                  offset: Offset(-3.0, 3.0),
                ),
              ],
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => HomeScreen(),
              ),
              (route) => false,
            );
          },
        ),
      ),
      body: Column(
        children: <Widget>[
          Center(
            child: Text(
              'Ayuda',
              style: GoogleFonts.anton(
                textStyle: Theme.of(context).textTheme.headline5,
                color: Color(0xFF45818E),
                shadows: [
                  Shadow(
                    color: Colors.black,
                    offset: Offset(-1.0, 1.0),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: alto * 0.06),
          ListTile(
            isThreeLine: true,
            leading:
                Icon(Icons.help_outline, color: Colors.cyan[800], size: 30),
            title: Text('¿Como se usa?'),
            subtitle: Text('¿Dudas sobre como usar la aplicación?'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HowToUse(),
                  ));
            },
          ),
          SizedBox(height: alto * 0.025),
          ListTile(
            leading: Icon(Icons.slideshow, color: Colors.cyan[800], size: 30),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetallesComer(),
                  ));
            },
            title: Text('Detalles del Comercio'),
            subtitle:
                Text('¿Como puedo ver los detalles y datos de un Comercio?'),
          ),
          SizedBox(height: alto * 0.06),
          ListTile(
            leading: Icon(Icons.slideshow, color: Colors.cyan[800], size: 30),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => OnBoarding(repite: true),
                  ));
            },
            title: Text('Ver otra vez la intro'),
          ),
        ],
      ),
    );
  }
}
