import 'package:flushbar/flushbar.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get_it/get_it.dart';
import 'package:pedime/models/user.dart';
// import 'package:pedime/ui/home.dart';
import 'package:pedime/ui/lista_del_comercio.dart';
import 'package:pedime/util/pedime_icons.dart';
import 'package:pedime/widgets/empty_content.dart';
import 'package:pedime/data/carrito.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

class VerCarrito extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _listComp = Provider.of<CartModel>(context);

    if (_listComp.items.length == 0) {
      return Material(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: EmptyContent(
            title: 'Todavía no hay artículos para pedir',
            message: String.fromCharCode(0x1F40C),
          ),
        ),
      );
    } else {
      return Material(
        color: Colors.grey[300],
        child: _buildContents(context, _listComp),
      );
    }
  }

/*   _confirmarZero(BuildContext context, int idx, Producto prod) {
    bool borra = true;
    final snackBar = FlushBar(
      content: Text('La cantidad llegó a 0, se borra el producto'),
      duration: Duration(seconds: 2),
      action: SnackBarAction(
        label: 'No!',
        onPressed: () {
          prod.cantidad = 1;
        },
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
    if (borra) {
      Provider.of<CartModel>(context).removeOne(idx);
    } else {
      prod.cantidad = 1;
    }
  } */

  void _comparteCart(BuildContext context) {
    final wsap = Provider.of<CartModel>(context, listen: false).wsapp;
    final pedi = Provider.of<CartModel>(context, listen: false).items;
    final tot =
        Provider.of<CartModel>(context, listen: false).totGral.toString();
    if (!wsap) {
      return null;
    } else {
      User usuario = GetIt.I.get<User>();
      //Preguntar si envía por cadetería

      StringBuffer pido = StringBuffer();
      pido.write('Hola soy ${usuario.nombre}. Te pido: \n');

      for (int i = 0; i < pedi.length; i++) {
        pido.write(
            '* ${pedi[i]['cantidad']} ${pedi[i]['nombre']}, ${pedi[i]['total']}\$.\n');
      }
      pido.write('Por un total de $tot\$.\n');
      pido.write('Para entregar en: ${usuario.dire}.\n Gracias');

      Share.share(pido.toString());
      pido.clear();
    }
  }

  Widget _buildContents(BuildContext context, _lista) {
    final _listComp = Provider.of<CartModel>(context, listen: false);
    final com = Provider.of<CartModel>(context).comercio;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 15),
          Expanded(
            child: ListView.builder(
              // shrinkWrap: true,
              itemCount: _lista.items.length,
              itemBuilder: (context, index) {
                Map prod = _lista.items[index];
                return Column(
                  children: <Widget>[
                    Slidable(
                      actionPane: SlidableStrechActionPane(),
                      actions: <Widget>[
                        IconSlideAction(
                          caption: 'Quitar',
                          color: Colors.grey[300],
                          icon: Icons.delete,
                          onTap: () => _listComp.removeOne(index),
                        ),
                      ],
                      secondaryActions: <Widget>[
                        IconSlideAction(
                          caption: 'Quitar',
                          color: Colors.grey[300],
                          icon: Icons.delete,
                          onTap: () => _listComp.removeOne(index),
                        ),
                      ],
                      /* secondaryActions: <Widget>[
                        IconSlideAction(
                          // caption: 'Quitar',
                          color: Color(0xFF45818E),
                          icon: Icons.exposure_plus_1,
                          onTap: () => _listComp.items[index]['cantidad'] + 1,
                        ),
                        IconSlideAction(
                          // caption: 'Quitar',
                          color: Color(0xFFA2C0C7),
                          icon: Icons.exposure_neg_1,
                          onTap: () => _listComp.items[index]['cantidad'] + 1,
                        )
                      ], */
                      child: Container(
                        // margin: const EdgeInsets.only(bottom: 25),
                        child: InkWell(
                          onLongPress: () {
                            // _lista.removeOne(index);
                          },
                          child: Row(
                            children: <Widget>[
                              ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: FadeInImage.assetNetwork(
                                    image: prod['img'],
                                    height: 90,
                                    width: 90,
                                    fadeInDuration: Duration(milliseconds: 200),
                                    fadeOutDuration:
                                        Duration(milliseconds: 400),
                                    fadeOutCurve: Curves.easeInOut,
                                    fit: BoxFit.fill,
                                    placeholder: "assets/images/box.png"),
                              ),
                              SizedBox(width: 15),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      prod['nombre'],
                                      overflow: TextOverflow.ellipsis,
                                      style:
                                          Theme.of(context).textTheme.headline6,
                                    ),
                                    Text(
                                      'Precio:  ${prod['precio'].toString().replaceAll(".", ",")}\$',
                                      style: TextStyle(
                                          color:
                                              Theme.of(context).primaryColor),
                                    ),
                                    SizedBox(height: 16),
                                    Row(
                                      children: [
                                        Text(
                                          'Cantidad: ',
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        //TODO: cambiar cantidad de instancia carrito
/*                                         if (prod['cantidad'] > 0) { IconButton(
                                          icon: Icon(Icons.remove),
                                          onPressed: () => {},
                                          iconSize: 20,
                                          color: Colors.red,
                                          splashColor: Colors.redAccent,
                                          splashRadius: 16,
                                          padding: EdgeInsets.all(1),
                                        ) } else {Text('')}, */
                                        Text(
                                          prod['cantidad'].toString(),
                                          style: TextStyle(fontSize: 16),
                                        ),
/*                                         IconButton(
                                          icon: Icon(Icons.add),
                                          onPressed: () => {},
                                          iconSize: 20,
                                          color: Colors.blue,
                                          splashColor: Colors.blueAccent,
                                          splashRadius: 16,
                                        ), */
                                      ],
                                    ),
/*                             GzIncrementador(
                                    cant: prod.cantidad,
                                    onCantSelected: () {
                                      //prod.total = carri.totalCarrito;
                                      prod.total;
                                    },
                                    onCantChange: (int val) {
                                      prod.cantidad = val;
                                      if (prod.cantidad == 0) {
                                        _confirmarZero(index, prod);
                                      }
                                    },
                                  ), */
                                  ],
                                ),
                              ),
                              Text(
                                  '${prod['total'].toString().replaceAll(".", ",")}\$',
                                  style: Theme.of(context).textTheme.headline5)
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 15),
                  ],
                );
              },
            ),
          ),
          Center(
              child: InkWell(
                  onTap: () => {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => ListaDelComercio(comId: com)))
                      },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Agregar mas Productos',
                      style: TextStyle(fontSize: 18),
                    ),
                  ))),
          Divider(
            color: Theme.of(context).accentColor,
          ),
          Row(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text('Vaciar'),
                  IconButton(
                    icon: Icon(Icons.remove_shopping_cart),
                    onPressed: () {
                      _listComp.removeAll();
                    },
                  )
                ],
              ),
              SizedBox(width: 25),
              _listComp.wsapp
                  ? Expanded(
                      // height: 50,
                      child: RaisedButton(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Pedir por",
                              style: TextStyle(fontSize: 18),
                            ),
                            SizedBox(width: 10),
                            Icon(PediMe.whatsapp),
                          ],
                        ),
                        onPressed: () => _comparteCart(context),
                        color: Theme.of(context).primaryColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                      ),
                    )
                  : Expanded(
                      // height: 50,
                      child: RaisedButton(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Pedir por",
                              style: TextStyle(fontSize: 18),
                            ),
                            SizedBox(width: 10),
                            // Spacer(),
                            Icon(Icons.phone_in_talk),
                          ],
                        ),
                        onPressed: () => {
                          Flushbar(
                            backgroundColor:
                                Theme.of(context).toggleableActiveColor,
                            title: 'Solo telefónico',
                            message:
                                'En este comercio, tenés que llamar y hacer el pedido tal como rellenaste en el carrito',
                            duration: Duration(seconds: 4),
                          )..show(context),
                        },
                        color: Theme.of(context).primaryColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                      ),
                    ),
              SizedBox(width: 25),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("TOTAL:",
                        style: Theme.of(context).textTheme.subtitle1),
                    Text("${_lista.totGral.toString().replaceAll(".", ",")}\$ ",
                        style: Theme.of(context).textTheme.headline5),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
