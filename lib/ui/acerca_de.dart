import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'package:pedime/ui/howTo.dart';
// import 'package:pedime/ui/onBoarding.dart';
import 'package:pedime/util/device.dart';
import 'package:pedime/util/pedime_icons.dart';
import 'package:pedime/widgets/app_icon.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

class AcercaScreen extends StatelessWidget {
  const AcercaScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final ancho = DeviceUtils.getScaledWidth(context, 1);
    final alto = DeviceUtils.getHeightWithoutStatusAnToolBar(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('PediMe'),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                // SizedBox(height: 70),
                Text(
                  'PediMe',
                  style: GoogleFonts.anton(
                    textStyle: Theme.of(context).textTheme.headline2,
                    color: Colors.white,
                    shadows: [
                      Shadow(
                        color: Colors.cyan[800],
                        offset: Offset(-4.0, 4.0),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: alto * 0.03),
                ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child:
                        AppIconWidget(image: 'assets/icons/ic_launcher.png')),
                Text(
                  'Vos pedís, vos tenes 😉',
                  style: TextStyle(fontSize: 22.0, color: Colors.black54),
                ),
                SizedBox(height: alto * 0.04),
                Text(
                  'Una aplicación de ',
                  style: TextStyle(fontSize: 16),
                ),
                InkWell(
                    child: Container(
                        width: 90,
                        height: 26,
                        child: ColoredBox(
                          color: Colors.cyan[300],
                          child: Center(
                              child: Text(
                            "GZSOFT",
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                letterSpacing: 1.7),
                            // textAlign: TextAlignVertical.center,
                          )),
                        )),
                    onTap: () async {
                      if (await canLaunch("https://gzsoftw.web.app/")) {
                        await launch("https://gzsoftw.web.app/");
                      } else {
                        throw 'No puedo llegar a https://gzsoftw.web.app/ .\n¿Problemas de Red?';
                      }
                    }),
                SizedBox(height: alto * 0.11),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Compartíla por ",
                      style: TextStyle(
                          // fontWeight: FontWeight.w600,
                          fontSize: 18),
                      // textAlign: TextAlignVertical.center,
                    ),
                    // SizedBox(width: 2),
                    FlatButton.icon(
                      label: Text('Whatsapp'),
                      color: Colors.grey[100],
                      // color: Theme.of(context).primaryColor,
                      icon: Icon(PediMe.whatsapp),
                      onPressed: () async {
                        StringBuffer mira = StringBuffer();
                        mira.write('Descargá PediMe, está buenísima! ');
                        mira.write(
                            "https://play.google.com/store/apps/details?id=com.gzsoft.pedime");
                        Share.share(mira.toString());
                        mira.clear();
                      },
                    )
                  ],
                ),
                SizedBox(height: alto * 0.05),
                Text('¿Sos comerciante?'),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Empezá a vender desde '),
                    InkWell(
                        child: Text("PediMe Web Admin ",
                            style: TextStyle(
                                color: Theme.of(context).primaryColor)),
                        onTap: () async {
                          if (await canLaunch("https://pedime-app.web.app")) {
                            await launch("https://pedime-app.web.app");
                          } else {
                            throw 'No puedo llegar a https://pedime-app.web.app.\n¿Problemas de Red?';
                          }
                        }),
                  ],
                ),
                SizedBox(height: alto * 0.10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Iconos diseñados por '),
                    InkWell(
                        child: Text("Itim2101 en FlatIcon ",
                            style: TextStyle(
                                color: Theme.of(context).primaryColor)),
                        onTap: () async {
                          if (await canLaunch(
                              "https://www.flaticon.com/authors/itim2101")) {
                            await launch(
                                "https://www.flaticon.com/authors/itim2101");
                          } else {
                            throw 'No puedo llegar a https://www.flaticon.com/authors/itim2101 .\n¿Problemas de Red?';
                          }
                        }),
                  ],
                ),
                SizedBox(height: alto * 0.01),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Fuentes by '),
                    InkWell(
                        child: Text("Google Fonts",
                            style: TextStyle(
                                color: Theme.of(context).primaryColor)),
                        onTap: () async {
                          if (await canLaunch("https://fonts.google.com/")) {
                            await launch("https://fonts.google.com/");
                          } else {
                            throw 'No puedo llegar a https://fonts.google.com/ .\n¿Problemas de Red?';
                          }
                        }),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
