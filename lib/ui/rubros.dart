import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:pedime/models/rubro.dart';
import 'package:pedime/ui/lista_comercios.dart';

class RubrosScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream:
          Firestore.instance.collection('rubros').orderBy('nombre').snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return LinearProgressIndicator();
        return _buildList(context, snapshot.data.documents);
      },
    );
  }

  Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 10.0),
      children: snapshot.map((data) => _buildListItem(context, data)).toList(),
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot data) {
    final rubro = Rubro.fromSnapshot(data);
    return Card(
      borderOnForeground: true,
      elevation: 4.0,
      margin: EdgeInsets.all(6.0),
      child: InkWell(
        onTap: () => {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ListaComercios(rubroId: rubro.nombre),
            ),
          ),
        },
        child: Padding(
          padding: const EdgeInsets.all(1.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Stack(
                children: [
                  FadeInImage.assetNetwork(
                      image: rubro.img,
                      fadeInDuration: Duration(milliseconds: 600),
                      fadeOutDuration: Duration(milliseconds: 600),
                      fadeOutCurve: Curves.easeInOut,
                      fit: BoxFit.fill,
                      width: 100,
                      height: 100,
                      placeholder: "assets/images/business.png"),
                ],
              ),
              SizedBox(
                width: 25,
              ),
              Text(
                rubro.nombre,
                style: TextStyle(fontSize: 22.0, color: Colors.black54),
              ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
