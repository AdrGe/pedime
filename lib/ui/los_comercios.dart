import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
// import 'package:google_fonts/google_fonts.dart';
import 'package:pedime/models/comercio.dart';
import 'package:pedime/models/user.dart';
import 'package:pedime/ui/lista_del_comercio.dart';
import 'package:pedime/widgets/empty_content.dart';

class LosComercios extends StatelessWidget {
  Stream _comerList() {
    User usuario = GetIt.I.get<User>();
    final ciudad = usuario.miCiudad;
    Stream lista = Firestore.instance
        .collection('ciudades/$ciudad/comercios')
        .orderBy('nombre')
        .snapshots();
    return lista;
  }

  Widget _noHay() {
    User usuario = GetIt.I.get<User>();
    final ciudad = usuario.miCiudad;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: EmptyContent(
        title: 'Aún no se agregaron comercios en $ciudad',
        message: '🤷',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _comerList(),
      builder: (context, snapshot) {
        if (!snapshot.hasData ||
            snapshot.hasError ||
            snapshot.data.documents.isEmpty) {
          return Scaffold(
            body: _noHay(),
          );
        } else {
          return Scaffold(
            body: _buildList(context, snapshot.data.documents),
          );
        }
      },
    );
  }

  Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 10.0),
      children: snapshot.map((data) => _buildListItem(context, data)).toList(),
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot data) {
    final comer = Comercio.fromSnapshot(data);
    return Card(
      borderOnForeground: true,
      elevation: 4.0,
      margin: EdgeInsets.all(6.0),
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ListaDelComercio(comId: comer.id),
            ),
          );
        },
        child: Padding(
          padding: const EdgeInsets.all(6.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: FadeInImage.assetNetwork(
                    image: comer.logo,
                    fadeInDuration: Duration(milliseconds: 400),
                    fadeOutDuration: Duration(milliseconds: 300),
                    fadeOutCurve: Curves.easeInOut,
                    fit: BoxFit.cover,
                    width: 100,
                    height: 100,
                    placeholder: "assets/images/money.png"),
              ),
              SizedBox(width: 25),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    comer.nombre,
                    style: TextStyle(fontSize: 22.0, color: Colors.black54),
                  ),
                  Text(comer.rubro),
                ],
              ),
              Spacer()
            ],
          ),
        ),
      ),
    );
  }
}
