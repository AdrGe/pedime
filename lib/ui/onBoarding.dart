import 'package:flutter/material.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';
import 'package:pedime/const/slides.dart';
import 'package:pedime/ui/ayudaScreen.dart';
import 'package:pedime/ui/set_prefs.dart';

class OnBoarding extends StatelessWidget {
  const OnBoarding({this.repite});
  final bool repite;

  @override
  Widget build(BuildContext context) {
    return IntroViewsFlutter(
      Slides.onBoard,
      background: Colors.black.withOpacity(0.6),
      showNextButton: true,
      skipText: Text('Omitir'),
      nextText: Text('>'),
      backText: Text('<'),
      showBackButton: true,
      doneText: Text('Listo'),
      onTapDoneButton: () {
        if (repite ?? false) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => AyudaScreen(),
            ),
            (route) => false,
          );
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => SetPrefs(),
            ),
          );
        }
      },
    );
  }
}
