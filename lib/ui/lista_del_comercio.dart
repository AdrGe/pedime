import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:neon/neon.dart';
import 'package:pedime/data/carrito.dart';
import 'package:pedime/models/producto.dart';
import 'package:pedime/models/super_comercio.dart';
import 'package:pedime/ui/comercio_screen.dart';
import 'package:pedime/ui/producto_screen.dart';
import 'package:pedime/util/device.dart';
import 'package:pedime/util/pedime_icons.dart';
import 'package:provider/provider.dart';
//import 'container_transition.dart';

class ListaDelComercio extends StatefulWidget {
  const ListaDelComercio({@required this.comId});
  final String comId;

  @override
  _ListaDelComercioState createState() => _ListaDelComercioState();
}

class _ListaDelComercioState extends State<ListaDelComercio>
// with SingleTickerProviderStateMixin
{
  bool wsapp = true;
/*   AnimationController _animationController;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
        duration: Duration(milliseconds: 450), value: 10.0, vsync: this);
  } */

  Stream<Comercio> _getCom() {
    return Firestore.instance
        .collection("comercios")
        .document(widget.comId)
        .get()
        .then((snapshot) {
      try {
        return Comercio.fromMap(snapshot.data);
      } catch (e) {
        print(e);
        return null;
      }
    }).asStream();
  }

  void _agProd(String comId, CartModel carri, Producto prod, bool wsapp) {
    if (carri.comercio != null &&
        carri.comercio != '' &&
        comId != carri.comercio) {
      Flushbar(
        onTap: (_) {
          carri.removeAll();
        },
        backgroundColor: Theme.of(context).errorColor,
        title: 'Error',
        message:
            'No se puede agregar productos de distintos comercios 🤷 \n Toque para vaciar',
        duration: Duration(seconds: 4),
      )..show(context);
    } else {
      carri.add(prod);
      carri.comercio = comId;
      carri.wsapp = wsapp;
      Flushbar(
        backgroundColor: Theme.of(context).accentColor,
        title: 'Agregado',
        message: 'Se agregó el producto al pedido 👍',
        duration: Duration(seconds: 3),
      )..show(context);
    }
  }

  _muestraData(BuildContext context, Comercio com) {
    return AlertDialog(
        title: RichText(
          text: TextSpan(
            style: TextStyle(color: Colors.black, fontSize: 20),
            text: "En ",
            children: <TextSpan>[
              TextSpan(
                  text: '${com.rubro} ',
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16)),
              TextSpan(
                  text: com.nombre, style: TextStyle(color: Colors.redAccent))
            ],
          ),
        ),
        content: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              // Divider(),
              Text('Podés pagar con:',
                  style: Theme.of(context).textTheme.headline6),
              Row(
                children: <Widget>[
                  Icon(Icons.attach_money,
                      color: Theme.of(context).accentColor),
                  Text(
                    ' Efectivo',
                    style: TextStyle(color: Colors.black54),
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  Icon(Icons.credit_card,
                      color: com.metPago.tc
                          ? Theme.of(context).accentColor
                          : Colors.grey[200]),
                  Text(
                    ' Tarjeta de Crédito',
                    style: com.metPago.tc
                        ? TextStyle(color: Colors.black54)
                        : TextStyle(
                            decoration: TextDecoration.lineThrough,
                            color: Colors.black26),
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  Icon(Icons.credit_card,
                      color: com.metPago.td
                          ? Theme.of(context).accentColor
                          : Colors.grey[200]),
                  Text(
                    ' Tarjeta de Débito',
                    style: com.metPago.td
                        ? TextStyle(color: Colors.black54)
                        : TextStyle(
                            decoration: TextDecoration.lineThrough,
                            color: Colors.black26),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Icon(Icons.mobile_screen_share,
                      color: com.metPago.mp
                          ? Theme.of(context).accentColor
                          : Colors.grey[200]),
                  Text(
                    ' Mercado Pago',
                    style: com.metPago.mp
                        ? TextStyle(color: Colors.black54)
                        : TextStyle(
                            decoration: TextDecoration.lineThrough,
                            color: Colors.black26),
                  )
                ],
              ),
              SizedBox(height: 12),
              Divider(),
              Text(' Tipos de Entrega',
                  style: Theme.of(context).textTheme.headline6),
              Row(
                children: <Widget>[
                  Icon(Icons.motorcycle,
                      color: com.metDeli.moto
                          ? Theme.of(context).accentColor
                          : Colors.grey[200]),
                  Text(
                    ' Envía por moto',
                    style: com.metDeli.moto
                        ? TextStyle(color: Colors.black54)
                        : TextStyle(
                            decoration: TextDecoration.lineThrough,
                            color: Colors.black26),
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  Icon(Icons.store,
                      color: com.metDeli.retir
                          ? Theme.of(context).accentColor
                          : Colors.grey[200]),
                  Text(
                    ' Retiras en puerta',
                    style: com.metDeli.retir
                        ? TextStyle(color: Colors.black54)
                        : TextStyle(
                            decoration: TextDecoration.lineThrough,
                            color: Colors.black26),
                  )
                ],
              ),
              SizedBox(height: 12),
              Divider(),
              Text('Pediles por', style: Theme.of(context).textTheme.headline6),
              Row(
                children: <Widget>[
                  Icon(Icons.phone,
                      color: com.pedirPor.telefono
                          ? Theme.of(context).accentColor
                          : Colors.grey[200]),
                  Text(
                    ' Teléfono',
                    style: com.pedirPor.telefono
                        ? TextStyle(color: Colors.black54)
                        : TextStyle(
                            decoration: TextDecoration.lineThrough,
                            color: Colors.black26),
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  Icon(PediMe.whatsapp,
                      color: com.pedirPor.whatsapp
                          ? Theme.of(context).accentColor
                          : Colors.grey[200]),
                  Text(
                    ' Whatsapp',
                    style: com.pedirPor.whatsapp
                        ? TextStyle(color: Colors.black54)
                        : TextStyle(
                            decoration: TextDecoration.lineThrough,
                            color: Colors.black26),
                  )
                ],
              ),
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<CartModel>(context);

    return Scaffold(
      body: StreamBuilder(
        stream: _getCom(),
        builder: (context, snapshot) {
          Comercio com = snapshot.data;
          if (!snapshot.hasData) {
            return Container(
              width: DeviceUtils.getScaledWidth(context, 1),
              height: 160,
              child: ColoredBox(
                color: Color(0xFF45818E),
                child: Text(
                  '\n  PediMe',
                  style: GoogleFonts.anton(
                    textStyle: Theme.of(context).textTheme.headline4,
                    color: Color(0xFF45818E),
                    shadows: [
                      Shadow(
                        color: Colors.black,
                        offset: Offset(-3.0, 3.0),
                      ),
                    ],
                  ),
                ),
              ),
            );
          } else {
            com.pedirPor.whatsapp ? cart.wsapp = true : cart.wsapp = false;
            return CustomScrollView(
              physics: const BouncingScrollPhysics(),
              slivers: <Widget>[
                SliverAppBar(
                  stretch: true,
                  pinned: true,
                  shape: ContinuousRectangleBorder(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(30),
                          bottomRight: Radius.circular(30))),
/*                   onStretchTrigger: () {
                    // Function callback for stretch
                    return;
                  }, */
                  expandedHeight: 200.0,
                  flexibleSpace: FlexibleSpaceBar(
                    stretchModes: <StretchMode>[
                      StretchMode.zoomBackground,
                      StretchMode.blurBackground,
                      StretchMode.fadeTitle,
                    ],
                    centerTitle: true,
                    title: InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                DescripcionComercio(comId: widget.comId),
                          ),
                        );
                      },
                      child: Neon(
                        text: com.nombre,
                        color: Colors.lightGreen,
                        fontSize: 25,
                        font: NeonFont.LasEnter,
                        flickeringText: true,
                        flickeringLetters: null,
                        glowingDuration: Duration(seconds: 280),
                      ),
                    ),
                    background: Stack(
                      fit: StackFit.loose,
                      children: [
                        DecoratedBox(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment(0.0, 0.8),
                              end: Alignment(0.0, 0.0),
                              colors: <Color>[
                                Color(0x60000000),
                                Color(0x00000000),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 50),
                        Center(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: FadeInImage.assetNetwork(
                                image: com.logo,
                                height: 150,
                                width: 150,
                                // fit: BoxFit.none,
                                fadeInDuration: Duration(milliseconds: 400),
                                fadeOutDuration: Duration(milliseconds: 300),
                                fadeOutCurve: Curves.easeInOut,
                                placeholder: "assets/images/money.png"),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Column(
                    children: <Widget>[
                      Center(
                        child: InkWell(
                          onTap: () {
                            showDialog(
                              context: context,
                              barrierDismissible: true,
                              builder: (BuildContext context) {
                                return _muestraData(context, com);
                              },
                            );
                          },
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text('Pagos: '),
                              Icon(Icons.attach_money,
                                  color: Theme.of(context).accentColor),
                              Icon(Icons.credit_card,
                                  color: com.metPago.tc
                                      ? Theme.of(context).accentColor
                                      : Colors.grey[200]),
                              Icon(Icons.credit_card,
                                  color: com.metPago.td
                                      ? Theme.of(context).accentColor
                                      : Colors.grey[200]),
                              Icon(Icons.mobile_screen_share,
                                  color: com.metPago.mp
                                      ? Theme.of(context).accentColor
                                      : Colors.grey[200]),
                              SizedBox(width: 2),
                              Text('|',
                                  style:
                                      TextStyle(fontWeight: FontWeight.w800)),
                              Text(' Del: '),
                              Icon(Icons.motorcycle,
                                  color: com.metDeli.moto
                                      ? Theme.of(context).accentColor
                                      : Colors.grey[200]),
                              SizedBox(width: 2),
                              Icon(
                                Icons.store,
                                color: com.metDeli.retir
                                    ? Theme.of(context).accentColor
                                    : Colors.grey[200],
                              ),
                              SizedBox(width: 3),
                              Text('|',
                                  style:
                                      TextStyle(fontWeight: FontWeight.w800)),
                              Text(' Pedir: '),
                              Icon(Icons.phone,
                                  color: com.pedirPor.telefono
                                      ? Theme.of(context).accentColor
                                      : Colors.grey[200]),
                              SizedBox(width: 2),
                              Icon(PediMe.whatsapp,
                                  color: com.pedirPor.whatsapp
                                      ? Theme.of(context).accentColor
                                      : Colors.grey[200]),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 5)
                    ],
                  ),
                ),
                SliverFillRemaining(
                  child: StreamBuilder<QuerySnapshot>(
                    stream: Firestore.instance
                        .collection('comercios/${widget.comId}/productos')
                        .orderBy('nombre')
                        .where('visible', isEqualTo: true)
                        .snapshots(),
                    builder: (context, snapshot) {
                      if (snapshot.hasError) {
                        return Text('Error: ${snapshot.error}');
                      } else if (!snapshot.hasData) {
                        return _noHay();
                      } else {
                        return _buildList(context, snapshot.data.documents);
                      }
                      /* switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          return _noHay();
                        case ConnectionState.waiting:
                          return LinearProgressIndicator();
                        case ConnectionState.active:
                          return LinearProgressIndicator();
                        case ConnectionState.done:
                          if (!snapshot.hasData) return _noHay();
                          return _buildList(context, snapshot.data.documents);
                          break;
                      }
                      return null;*/
                    },
                  ),
                ),
              ],
            );
          }
        },
      ),
    );
  }

  Widget _noHay() {
    return CustomScrollView(
      physics: const BouncingScrollPhysics(),
      slivers: <Widget>[
/*         SliverAppBar(
          shape: ContinuousRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30))),
          title: Text('Comercio'),
        ), */
        SliverList(
          delegate: SliverChildListDelegate(
            [
              ListTile(
                leading: Icon(Icons.do_not_disturb_on),
                title: Text('No hay productos'),
                subtitle: Text('El comercio aún no agregó sus productos 🤷'),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 10.0),
      children: snapshot.map((data) => _buildListItem(context, data)).toList(),
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot data) {
    final cart = Provider.of<CartModel>(context);
    final prod = Producto.fromSnapshot(data);
    return Card(
      borderOnForeground: true,
      elevation: 4.0,
      margin: EdgeInsets.all(6.0),
      child: InkWell(
        splashColor: Theme.of(context).primaryColorLight,
        onTap: () => {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => DescripcionProducto(
                comId: widget.comId,
                prod: prod,
                ws: wsapp,
              ),
            ),
          ),
        },
        child: Padding(
          padding: const EdgeInsets.all(6.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              //TODO: si no hay imagen da problemas
              Stack(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: FadeInImage.assetNetwork(
                        image: prod.img,
                        fadeInDuration: Duration(milliseconds: 200),
                        fadeOutDuration: Duration(milliseconds: 400),
                        fadeOutCurve: Curves.easeInOut,
                        fit: BoxFit.fill,
                        width: 100,
                        height: 100,
                        placeholder: "assets/images/box.png"),
                  ),
                ],
              ),
              Expanded(
                flex: 5,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(children: <Widget>[
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        prod.nombre,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ),
                    SizedBox(height: 4),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        prod.desc,
                        maxLines: 3,
                        softWrap: true,
                        textAlign: TextAlign.start,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ]),
                ),
              ),
              Spacer(),
              Column(
                children: <Widget>[
                  Text('${prod.precio.toString().replaceAll(".", ",")}\$',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                  IconButton(
                    onPressed: () {
                      // this._animationController.forward();
                      _agProd(widget.comId, cart, prod, wsapp);
                    },
                    iconSize: 24,
                    icon: Icon(
                      Icons.pan_tool,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  Text(
                    'Quiero',
                    style: TextStyle(
                        color: Theme.of(context).primaryColor, fontSize: 9),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
