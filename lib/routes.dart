import 'package:flutter/material.dart';

import 'ui/set_prefs.dart';
import 'ui/home.dart';
import 'ui/pedido.dart';
import 'ui/rubros.dart';

class Routes {
  Routes._();
  //static variables
  static const String splash = '/splash';
  static const String rubros = '/rubros';
  static const String home = '/home';
  static const String comercios = '/com';
  static const String comerXru = '/cxr';
  static const String pedido = '/pedido';
  static const String configs = '/configs';

  static final routes = <String, WidgetBuilder>{
    rubros: (BuildContext context) => RubrosScreen(),
    home: (BuildContext context) => HomeScreen(),
    // comerXru: (BuildContext context) => ComerciosXRubroScreen(),
    pedido: (BuildContext context) => VerCarrito(),
    configs: (BuildContext context) => SetPrefs(),
  };
}
