class Preferences {
  Preferences._();

  static const String is_dark_mode = "is_dark_mode";
  static const String nro_pedido = "nro_pedido";
  static const String nombre_usuario = "nombre_usuario";
  static const String direccion = "direccion";
  static const String ciudad = "ciudad";
  static const String configurado = 'configurado';
}
