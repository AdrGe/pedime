import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TuCiudad extends ChangeNotifier {
  String _appCiudad;

  String get appCiudad => _appCiudad;

  traeCiudad() async {
    await SharedPreferences.getInstance()
        .then((prefs) => prefs.getString('ciudad') ?? '');
  }

  void changeCiudad(String city) async {
    var prefs = await SharedPreferences.getInstance();
    if (_appCiudad == city) {
      return;
    } else {
      _appCiudad = city;
      await prefs.setString('ciudad', _appCiudad);
      notifyListeners();
    }
  }
}
