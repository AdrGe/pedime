import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'const.dart';

class SharedPreferenceHelper with ChangeNotifier {
  // constructor
  SharedPreferenceHelper();

  void setCiudad(String city) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('ciudad', city);
  }

  Future<String> getCiudad() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var ciu = prefs.getString('ciudad');
    return ciu;
    // notifyListeners();
  }

  // General Methods: ----------------------------------------------------------
  Future<String> get nroPedido async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(Preferences.nro_pedido);
  }

  Future<String> dameString(String clave) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(clave).toString();
  }

  Future<void> guardaString(String clave, String valor) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(clave, valor);
  }

  Future<void> saveUltimoPedido(int nroPedi) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(Preferences.nro_pedido, nroPedi);
  }

  Future<String> get nombreUsuario async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(Preferences.nombre_usuario);
  }

  Future<void> setNombreUsuario(String nomb) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(Preferences.nombre_usuario, nomb);
  }

  Future<String> get direccion async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(Preferences.direccion);
  }

  Future<void> setDireccion(String dire) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(Preferences.direccion, dire);
  }

  Future<void> setConfigurado(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(Preferences.configurado, value);
  }

  // Theme:------------------------------------------------------
  Future<bool> get isDarkMode async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(Preferences.is_dark_mode) ?? false;
  }

  Future<void> changeBrightnessToDark(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(Preferences.is_dark_mode, value);
  }
}
