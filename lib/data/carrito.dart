import 'dart:collection';
import 'package:flutter/foundation.dart';
// import 'package:pedime/models/prod_pedido.dart';
import 'package:pedime/models/producto.dart';

class CartModel extends ChangeNotifier {
  /// Internal, private state of the cart.
  final List<Map> _items = [];

  /// An unmodifiable view of the items in the cart.
  UnmodifiableListView<Map> get items => UnmodifiableListView(_items);

  String comercio;
  String get getComercio => comercio;

  set setComercio(String comercio) => this.comercio = comercio;
  bool wsapp;

  double totGral = 0.0;

/*
   genPedi(){
    List<Producto> pedido;
    Producto prod;
    if (_items.length > 0) {
      for (int i = 0; i < _items.length; i++) {
        prod.nomb = _items[i]['nombre'];
        prod.cantidad = _items[i]['cantidad'];
        prod.total = (_items[i]['precio'] * _items[i]['cantidad']);
        pedido.add(prod);
      }
    } else {
      return pedido;
    }
  } */
  /// Prepara el carrito para pedir por WAP
  genPedido() {
    List<Map> pedido; /*  = []; */
    Map prod;
    if (_items.length > 0) {
      for (int i = 0; i < _items.length; i++) {
        prod['nomb'] = _items[i]['nombre'];
        prod['cantidad'] = _items[i]['cantidad'];
        prod['total'] = (_items[i]['precio'] * _items[i]['cantidad']);
        pedido.add(prod);
      }
    }
    return pedido;
  }

  /// Costo total de los items[] en el carrito
  double getTotalCarrito() {
    double totalCarrito = 0.0;
    for (int i = 0; i < _items.length; i++) {
      totalCarrito += _items[i]['total'];
    }
    return totalCarrito;
  }

  /// Agrega [item] al carrito. Estos métodos son la única manera de modificar al carrito desde "afuera"
  void add(Producto item) {
    Map paq = item.toMap();
    paq['nombre'] = item.nombre;
    paq['cantidad'] ??= 1;
    paq['total'] = paq['cantidad'] * item.precio;
    if (_items.length == 0) {
      _items.add(paq);
    } else {
      bool encontrado = false;
      for (int i = 0; i < _items.length; i++) {
        if (item.id == _items[i]['id']) {
          _items[i]['cantidad'] += 1;
          _items[i]['total'] = _items[i]['cantidad'] * item.precio;
          encontrado = true;
          break;
        }
      }
      !encontrado ? _items.add(paq) : encontrado = false;
    }
    totGral = getTotalCarrito();
    notifyListeners();
  }

  /// Quita un [item] del carrito.
  void removeOne(int idx) {
    _items.removeAt(idx);
    totGral = getTotalCarrito();
    if (_items.length == 0) {
      this.comercio = '';
    }
    notifyListeners();
  }

  /// Vacía el carrito.
  void removeAll() {
    _items.clear();
    this.comercio = '';
    notifyListeners();
  }
}
