import 'dart:async';
import 'package:meta/meta.dart';

import 'package:pedime/models/comercio.dart';
import 'package:pedime/models/producto.dart';
import 'package:pedime/models/rubro.dart';
import 'firestore_paths.dart';
import 'firestore_service.dart';

// String documentIdFromCurrentDate() => DateTime.now().toIso8601String();

class FirestoreDatabase {
  FirestoreDatabase();
  final _service = FirestoreService.instance;

  // Stream<List<Entry>> entriesStream({Job job}) =>
  //   _service.collectionStream<Entry>(
  //     path: FirestorePath.entries(uid),
  //     queryBuilder: job != null
  //         ? (query) => query.where('jobId', isEqualTo: job.id)
  //         : null,
  //     builder: (data, documentID) => Entry.fromMap(data, documentID),
  //     sort: (lhs, rhs) => rhs.start.compareTo(lhs.start),

  // objects.sort((a, b) {
  // return a.value['name'].toString().toLowerCase().compareTo(b.value['name'].toString().toLowerCase());
// });
  //   );

  Stream<List<Rubro>> rubrosStream() => _service.collectionStream(
      path: FirestorePath.rubros(),
      builder: (data) => Rubro.fromMap(data),
      sort: (lhs, rhs) => rhs.nombre.compareTo(lhs.nombre));
  // lhs.nombre.toLowerCase().compareTo(rhs.nombre.toLowerCase()));

  Stream<List<Comercio>> comerciosStream(String ciudad) =>
      _service.collectionStream(
        path: 'ciudades/$ciudad/comercios',
        builder: (data) => Comercio.fromMap(data),
      );

  Stream<Producto> prodStream(
          {@required String comId, @required String prodId}) =>
      _service.documentStream(
        path: FirestorePath.unProducto(comId, prodId),
        builder: (data, documentId) => Producto.fromMap(data),
      );

  Stream<List<Producto>> prodsStream({@required String comId}) =>
      _service.collectionStream(
        // path: FirestorePath.productosDelComer(comId),
        path: 'comercios/$comId/productos',
        builder: (data) => Producto.fromMap(data),
      );

  Stream<List<Comercio>> comerXRubroStream(
          {@required String city, @required String rubroId}) =>
      _service.collectionStream(
        path: FirestorePath.comXrubro(city, rubroId),
        builder: (data) => Comercio.fromMap(data),
      );

/*   Stream<List<Entry>> entriesStream({Job job}) =>
      _service.collectionStream<Entry>(
        path: FirestorePath.entries(uid),
        queryBuilder: job != null
            ? (query) => query.where('jobId', isEqualTo: job.id)
            : null,
        builder: (data, documentID) => Entry.fromMap(data, documentID),
        sort: (lhs, rhs) => rhs.start.compareTo(lhs.start),
      ); */
}
