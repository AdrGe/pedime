class FirestorePath {
// Colecciones
  static String rubros() => 'rubros';
  static String comercios(String city) => 'ciudades/$city/comercios';
  static String comXrubro(String city, String rubroId) =>
      'ciudades/$city/rubros/$rubroId/comercios';
  static String productosDelComer(String comId) => 'comercios/$comId/productos';

// Documentos
  static String unComercio(String comId) => 'comercios/$comId';
  static String unProducto(String prodId, String comId) =>
      'comercios/$comId/productos/$prodId';
}
