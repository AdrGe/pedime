class ProdPedi {
  String nomb;
  int cant;
  double total;

  ProdPedi(this.nomb, this.cant, this.total);

  ProdPedi.fromJson(Map<String, dynamic> json)
      : nomb = json['nomb'],
        cant = json['cant'],
        total = json['total'];

  Map<String, dynamic> toJson() => {'nomb': nomb, 'cant': cant, 'total': total};
}
