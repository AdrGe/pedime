import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class User extends ChangeNotifier {
  String nombre;
  String dire;
  String refe;
  String ciudad;
  bool configurado;

  User({
    this.nombre,
    this.dire,
    this.refe,
    this.ciudad,
    this.configurado,
  });

  User copyWith({
    String nombre,
    String dire,
    String refe,
    String ciudad,
    bool configurado,
  }) =>
      User(
        nombre: nombre ?? this.nombre,
        dire: dire ?? this.dire,
        refe: refe ?? this.refe,
        ciudad: ciudad ?? this.ciudad,
        configurado: configurado ?? this.configurado,
      );

  factory User.fromJson(String str) => User.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  String get miCiudad {
    return this.ciudad;
  }

  factory User.fromMap(Map<String, dynamic> json) => User(
        nombre: json["nombre"],
        dire: json["dire"],
        refe: json["refe"],
        ciudad: json["ciudad"],
        configurado: json["configurado"],
      );

  Map<String, dynamic> toMap() => {
        "nombre": nombre,
        "dire": dire,
        "refe": refe,
        "ciudad": ciudad,
        "configurado": configurado,
      };

  void cambiaConf() async {
    var prefs = await SharedPreferences.getInstance();
    configurado = true;
    await prefs.setBool('configurado', true);
    notifyListeners();
  }

  void nuevaCiudad(String city) async {
    var prefs = await SharedPreferences.getInstance();
    if (ciudad == city) {
      return;
    } else {
      ciudad = city;
      await prefs.setString('ciudad', ciudad);
      notifyListeners();
    }
  }

  void nuevoNombre(String nomb) async {
    var prefs = await SharedPreferences.getInstance();
    if (nombre == nomb) {
      return;
    } else {
      nombre = nomb;
      await prefs.setString('nombre', nomb);
      notifyListeners();
    }
  }

  void nuevaDire(String dir) async {
    var prefs = await SharedPreferences.getInstance();
    if (dire == dir) {
      return;
    } else {
      dire = dir;
      await prefs.setString('dire', dir);
      notifyListeners();
    }
  }
}
