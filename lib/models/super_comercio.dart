import 'dart:convert';

class Comercio {
  final MetDeli metDeli;
  final String calle;
  final String logo;
  final int cel;
  final int nro;
  final int tel;
  final MetPago metPago;
  final PedirPor pedirPor;
  final String nombre;
  final String rubro;
  final String ciudad;
  Comercio({
    this.metDeli,
    this.calle,
    this.logo,
    this.cel,
    this.nro,
    this.tel,
    this.metPago,
    this.pedirPor,
    this.nombre,
    this.rubro,
    this.ciudad,
  });

  Comercio copyWith({
    MetDeli metDeli,
    String calle,
    String logo,
    int cel,
    int nro,
    int tel,
    MetPago metPago,
    PedirPor pedirPor,
    String nombre,
    String rubro,
    String ciudad,
  }) {
    return Comercio(
      metDeli: metDeli ?? this.metDeli,
      calle: calle ?? this.calle,
      logo: logo ?? this.logo,
      cel: cel ?? this.cel,
      nro: nro ?? this.nro,
      tel: tel ?? this.tel,
      metPago: metPago ?? this.metPago,
      pedirPor: pedirPor ?? this.pedirPor,
      nombre: nombre ?? this.nombre,
      rubro: rubro ?? this.rubro,
      ciudad: ciudad ?? this.ciudad,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'metDeli': metDeli?.toMap(),
      'calle': calle,
      'logo': logo,
      'cel': cel,
      'nro': nro,
      'tel': tel,
      'metPago': metPago?.toMap(),
      'pedirPor': pedirPor?.toMap(),
      'nombre': nombre,
      'rubro': rubro,
      'ciudad': ciudad,
    };
  }

  static Comercio fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Comercio(
      metDeli: MetDeli.fromMap(map['metDeli']),
      calle: map['calle'],
      logo: map['logo'],
      cel: map['cel']?.toInt(),
      nro: map['nro']?.toInt(),
      tel: map['tel']?.toInt(),
      metPago: MetPago.fromMap(map['metPago']),
      pedirPor: PedirPor.fromMap(map['pedirPor']),
      nombre: map['nombre'],
      rubro: map['rubro'],
      ciudad: map['ciudad'],
    );
  }

  String toJson() => json.encode(toMap());

  static Comercio fromJson(String source) => fromMap(json.decode(source));

  @override
  String toString() {
    return 'Comercio(metDeli: $metDeli, calle: $calle, logo: $logo, cel: $cel, nro: $nro, tel: $tel, metPago: $metPago, pedirPor: $pedirPor, nombre: $nombre, rubro: $rubro, ciudad: $ciudad)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Comercio &&
        o.metDeli == metDeli &&
        o.calle == calle &&
        o.logo == logo &&
        o.cel == cel &&
        o.nro == nro &&
        o.tel == tel &&
        o.metPago == metPago &&
        o.pedirPor == pedirPor &&
        o.nombre == nombre &&
        o.rubro == rubro &&
        o.ciudad == ciudad;
  }

  @override
  int get hashCode {
    return metDeli.hashCode ^
        calle.hashCode ^
        logo.hashCode ^
        cel.hashCode ^
        nro.hashCode ^
        tel.hashCode ^
        metPago.hashCode ^
        pedirPor.hashCode ^
        nombre.hashCode ^
        rubro.hashCode ^
        ciudad.hashCode;
  }
}

class MetDeli {
  final bool retir;
  final bool moto;
  MetDeli({
    this.retir,
    this.moto,
  });

  MetDeli copyWith({
    bool retir,
    bool moto,
  }) {
    return MetDeli(
      retir: retir ?? this.retir,
      moto: moto ?? this.moto,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'retir': retir,
      'moto': moto,
    };
  }

  static MetDeli fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return MetDeli(
      retir: map['retir'],
      moto: map['moto'],
    );
  }

  String toJson() => json.encode(toMap());

  static MetDeli fromJson(String source) => fromMap(json.decode(source));

  @override
  String toString() => 'MetDeli(retir: $retir, moto: $moto)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is MetDeli && o.retir == retir && o.moto == moto;
  }

  @override
  int get hashCode => retir.hashCode ^ moto.hashCode;
}

class MetPago {
  final bool Efectivo;
  final bool tc;
  final bool td;
  final bool mp;
  MetPago({
    this.Efectivo,
    this.tc,
    this.td,
    this.mp,
  });

  MetPago copyWith({
    bool Efectivo,
    bool tc,
    bool td,
    bool mp,
  }) {
    return MetPago(
      Efectivo: Efectivo ?? this.Efectivo,
      tc: tc ?? this.tc,
      td: td ?? this.td,
      mp: mp ?? this.mp,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'Efectivo': Efectivo,
      'tc': tc,
      'td': td,
      'mp': mp,
    };
  }

  static MetPago fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return MetPago(
      Efectivo: map['Efectivo'],
      tc: map['tc'],
      td: map['td'],
      mp: map['mp'],
    );
  }

  String toJson() => json.encode(toMap());

  static MetPago fromJson(String source) => fromMap(json.decode(source));

  @override
  String toString() {
    return 'MetPago(Efectivo: $Efectivo, tc: $tc, td: $td, mp: $mp)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is MetPago &&
        o.Efectivo == Efectivo &&
        o.tc == tc &&
        o.td == td &&
        o.mp == mp;
  }

  @override
  int get hashCode {
    return Efectivo.hashCode ^ tc.hashCode ^ td.hashCode ^ mp.hashCode;
  }
}

class PedirPor {
  final bool telefono;
  final bool whatsapp;
  PedirPor({
    this.telefono,
    this.whatsapp,
  });

  PedirPor copyWith({
    bool telefono,
    bool whatsapp,
  }) {
    return PedirPor(
      telefono: telefono ?? this.telefono,
      whatsapp: whatsapp ?? this.whatsapp,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'telefono': telefono,
      'whatsapp': whatsapp,
    };
  }

  static PedirPor fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return PedirPor(
      telefono: map['telefono'],
      whatsapp: map['whatsapp'],
    );
  }

  String toJson() => json.encode(toMap());

  static PedirPor fromJson(String source) => fromMap(json.decode(source));

  @override
  String toString() => 'PedirPor(telefono: $telefono, whatsapp: $whatsapp)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is PedirPor && o.telefono == telefono && o.whatsapp == whatsapp;
  }

  @override
  int get hashCode => telefono.hashCode ^ whatsapp.hashCode;
}
