// To parse this JSON data, do
//
//     final comercio = comercioFromJson(jsonString);

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';
import 'dart:convert';

class Comercio {
  String id;
  String nombre;
  String logo;
  String rubro;
  DocumentReference reference;

  Comercio({
    this.id,
    @required this.nombre,
    this.logo,
    @required this.rubro,
    this.reference,
  });

  Comercio copyWith({
    String id,
    String nombre,
    String logo,
    String rubro,
  }) =>
      Comercio(
        id: id ?? this.id,
        nombre: nombre ?? this.nombre,
        logo: logo ?? this.logo,
        rubro: rubro ?? this.rubro,
      );

  Comercio.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data, reference: snapshot.reference);

  String toJson() => json.encode(toMap());

  Comercio.fromMap(Map<String, dynamic> map, {this.reference})
      : assert(map['nombre'] != null),
        id = map['id'],
        nombre = map['nombre'],
        logo = map['logo'],
        rubro = map['rubro'];

  Map<String, dynamic> toMap() =>
      {"id": id, "nombre": nombre, "logo": logo, "rubro": rubro};
}
