// To parse this JSON data, do
//
//     final rubro = rubroFromJson(jsonString);

import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';

class Rubro {
  // final String rId;
  final String nombre;
  final String img;
  final DocumentReference reference;

/*   Rubro({
    @required this.rId,
    @required this.nombre,
    @required this.img,
    this.reference
  }); */

/*   Rubro copyWith({
    String rId,
    String nombre,
    String img,
  }) =>
      Rubro(
        rId: rId ?? this.rId,
        nombre: nombre ?? this.nombre,
        img: img ?? this.img,
      ); */
  Map<String, dynamic> toMap() => {
        // "rId": rId,
        "nombre": nombre,
        "imgUrl": img,
      };

  Rubro.fromMap(Map<String, dynamic> map, {this.reference})
      :
        // : assert(map['rId'] != null),
        assert(map['nombre'] != null),
        assert(map["img"] != null),
        nombre = map['nombre'],
        img = map['img'];
  // rId = map['rId'];

  factory Rubro.fromJson(String str) => Rubro.fromMap(json.decode(str));

  Rubro.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data, reference: snapshot.reference);

  String toJson() => json.encode(toMap());
}
