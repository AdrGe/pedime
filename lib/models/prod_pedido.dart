import 'dart:convert';

class ProductoPedido {
  String nomb;
  int cantidad;
  int total;
  ProductoPedido({
    this.nomb,
    this.cantidad,
    this.total,
  });

  ProductoPedido copyWith({
    String nomb,
    int cantidad,
    int total,
  }) {
    return ProductoPedido(
      nomb: nomb ?? this.nomb,
      cantidad: cantidad ?? this.cantidad,
      total: total ?? this.total,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'nomb': nomb,
      'cantidad': cantidad,
      'total': total,
    };
  }

  static ProductoPedido fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return ProductoPedido(
      nomb: map['nomb'],
      cantidad: map['cantidad']?.toInt(),
      total: map['total']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  static ProductoPedido fromJson(String source) => fromMap(json.decode(source));

  @override
  String toString() =>
      'ProductoPedido(nomb: $nomb, cantidad: $cantidad, total: $total)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is ProductoPedido &&
        o.nomb == nomb &&
        o.cantidad == cantidad &&
        o.total == total;
  }

  @override
  int get hashCode => nomb.hashCode ^ cantidad.hashCode ^ total.hashCode;
}
