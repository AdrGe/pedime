// To parse this JSON data, do
//
//     final producto = productoFromJson(jsonString);

import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:convert';

class Producto {
  final String id;
  final String nombre;
  final String img;
  final double precio;
  final String desc;
  final DocumentReference reference;
  int cantidad;

  Producto({
    this.id,
    this.nombre,
    this.img,
    this.precio,
    this.cantidad,
    this.desc,
    this.reference,
  });

  static int i() => 1;

/*   double get total {
    return cantidad * precio;
  } */

  sumCantidad() => cantidad += 1;

  resCantidad() => cantidad -= 1;

  // set total(double val) => total;

  factory Producto.fromJson(String str) => Producto.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Producto.fromMap(Map<String, dynamic> json) => Producto(
        id: json["id"],
        nombre: json["nombre"],
        img: json["img"],
        precio: json["precio"],
        desc: json["desc"],
        cantidad: json["cantidad"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "nombre": nombre,
        "img": img,
        "precio": precio,
        "desc": desc,
        "cantidad": cantidad,
        // "total": total
      };

  Producto.fromSnapshot(DocumentSnapshot snapshot)
      : id = snapshot.documentID,
        nombre = snapshot['nombre'],
        img = snapshot['img'],
        precio = snapshot['precio'].toDouble(),
        desc = snapshot['desc'],
        // cantidad = this.cantidad,
        reference = snapshot.reference;

  // this.fromMap(snapshot.data, reference: snapshot.reference);
}
