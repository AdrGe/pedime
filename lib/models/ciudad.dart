import 'package:meta/meta.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Ciudad {
  String nombre;
  String codPost;
  String provincia;
  DocumentReference reference;

  Ciudad({
    @required this.nombre,
    this.codPost,
    this.provincia,
    this.reference,
  });

  Ciudad.fromMap(Map<String, dynamic> map, {this.reference})
      : assert(map['nombre'] != null),
        nombre = map['nombre'],
        codPost = map['codPost'],
        provincia = map['provincia'];

  Ciudad.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data, reference: snapshot.reference);

  Map<String, dynamic> toMap() => {
        "nombre": nombre,
        "codPost": codPost,
        "provincia": provincia,
      };
}
