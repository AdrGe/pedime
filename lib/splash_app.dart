import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pedime/const/app_theme.dart';
import 'package:get_it/get_it.dart';
import 'package:pedime/widgets/app_icon.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'models/user.dart';

class SplashApp extends StatefulWidget {
  final VoidCallback onInitializationComplete;
  // final bool initializationShouldFail;

  const SplashApp({
    Key key,
    @required this.onInitializationComplete,
    // this.initializationShouldFail = false,
  }) : super(key: key);

  @override
  _SplashAppState createState() => _SplashAppState();
}

class _SplashAppState extends State<SplashApp> {
  bool _hasError = false;

  User _localUser = GetIt.I.get<User>();

  @override
  void didChangeDependencies() {
    iniciaSettings();
    super.didChangeDependencies();
  }

  Future<void> iniciaSettings() async {
    final usuario = _localUser;
    await SharedPreferences.getInstance().then((prefs) {
      if ((prefs.getBool('configurado') ?? false) == false) {
        // prefs.setBool('configurado', false);
        usuario.configurado = false;
      } else {
        usuario.nombre = prefs.getString('nombre');
        usuario.ciudad = prefs.getString('ciudad');
        usuario.dire = prefs.getString('dire');
        usuario.configurado = true;
      }
    });
    Future.delayed(
      Duration(milliseconds: 1500),
      () => widget.onInitializationComplete(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PediMe',
      theme: themeData,
      home: _buildBody(),
    );
  }

  Widget _buildBody() {
    if (_hasError) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'PediMe',
            style: TextStyle(fontSize: 32.0, color: Colors.black54),
          ),
          SizedBox(height: 25),
          ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: AppIconWidget(image: 'assets/icons/ic_launcher.png')),
          RaisedButton(
            onPressed: () {},
            child: Text('Reintentar'),
          ),
        ],
      );
    }
    return Material(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'PediMe',
              style: GoogleFonts.anton(
                textStyle: Theme.of(context).textTheme.headline2,
                color: Colors.white,
                shadows: [
                  Shadow(
                    color: Colors.cyan[800],
                    offset: Offset(-4.0, 4.0),
                  ),
                ],
              ),
            ),
            SizedBox(height: 25),
            ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: AppIconWidget(image: 'assets/icons/ic_launcher.png')),
            // Spacer(),
            SizedBox(height: 45),
            Text(
              'Vos pedís, vos tenes 😉',
              style: TextStyle(fontSize: 22.0, color: Colors.black54),
            ),
            SizedBox(height: 45),
            CircularProgressIndicator(),
          ],
        ),
      ),
    );
  }
}
